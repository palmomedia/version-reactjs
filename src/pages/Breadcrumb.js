import React, { Component } from 'react';
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class BreadcrumbMy extends Component{
    render() {
        const paths = []
        var savedPath = ''
        window.location.pathname.split('/').reduce((prev, curr, index) => {
            savedPath += curr + '/'
            paths[index] = {url: savedPath, name: curr.toLowerCase().replace(/-/g, " ")}
            //console.log(index, savedPath)
            return paths[index]
        })
        return(
            <Breadcrumb className="arr-bread">
                <Breadcrumb.Item href="/"><FontAwesomeIcon icon="home" /> &nbsp;Home</Breadcrumb.Item>
                {paths.map(p => <Breadcrumb.Item key={p.url} href={`/${p.url}`}>{p.name}</Breadcrumb.Item>)}
            </Breadcrumb>
        )
    }
}

export default BreadcrumbMy;