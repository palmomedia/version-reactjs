import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Container from 'react-bootstrap/Container';
import CookieConsent from "react-cookie-consent";
import footerIcon from '../assets/palmomedia_icon_2020.png';

class Footer extends Component{
    render() {
        return(
            <footer>
                <div className="footer">
                <div className="footerme footer-custom1">
                    <Container>
                        <div className="row">
                            <div className="col-md-6 mt-md-0 mt-3" data-aos="zoom-out">
                                <h5 className="">palmomedia<br />a DevOps Company</h5>
                                <p><small>Wir verstehen uns als M&ouml;glichmacher im Full-Service Segment. <br />
                                Egal ob <b>legacy Software</b> oder Grünewiese, wir bauen Ihnen Ihre <b>Webapplikation</b> und helfen beim <b>Hosten</b>.</small></p>
                            </div>
                            <hr className="clearfix w-100 d-md-none pb-3" />
                            <div className="col-md-3 mb-md-0 mb-3" data-aos="flip-up">
                                <h5 className="text-uppercase">Pflicht</h5>
                                <ul className="list-unstyled">
                                <li>
                                    <a href="/impressum/">
                                        Impressum und Kontakt
                                    </a>
                                </li>
                                <li>
                                    <a href="/datenschutzerklaerung/">
                                        Datenschutzerkl&auml;rung
                                    </a>
                                </li>
                                </ul>
                            </div>
                            <div className="col-md-3 mb-md-0 mb-3" data-aos="flip-up">
                                <h5 className="text-uppercase">K&uuml;r</h5>
                                <ul className="list-unstyled">
                                <li>
                                    <a href="/sitemap">
                                        Sitemap
                                    </a>
                                </li>
                                <li>
                                    <a href="/uber-palmomedia">
                                        &Uuml;ber palmomedia
                                    </a>
                                </li>
                                <li>
                                    <a href="/blog">
                                        Blog
                                    </a>
                                </li>
                                <li>
                                    <a href="/service">
                                        Unser Service
                                    </a>
                                </li>
                                <li>
                                    <a href="https://gitlab.com/palmomedia/version-reactjs" target="_blank" rel="noopener noreferrer">GitLab</a>
                                </li>
                                </ul>
                            </div>
                        </div>
                    </Container>
                    <div className="footer-custom2">
                        <div className="footer-copyright py-3 text-center">
                            <small>
                                &copy; 2020 palmomedia                                
                                <br />made with <img src="https://www.shareicon.net/data/256x256/2016/07/08/117367_logo_512x512.png" height="20" alt="reactjs" /> and <FontAwesomeIcon icon={['fa', 'heartbeat']} /> in Berlin<br />
                                <a href="https://validator.w3.org/nu/?doc=http%3A%2F%2Freactjs.palmomedia.de%2F" target="_blank" title="html5 conform" rel="noopener noreferrer" className="footerlink">HTML</a> <FontAwesomeIcon icon={['fab', 'html5']} /> &nbsp;-&nbsp;
                                <FontAwesomeIcon icon={['fab', 'google']} /> <a href="https://developers.google.com/speed/pagespeed/insights/?hl=de%26url%3Dhttps%3A%2F%2Freactjs.palmomedia.de&url=https%3A%2F%2Freactjs.palmomedia.de%2F" rel="noopener noreferrer" target="_blank" title="Google PageSpeed" className="footerlink">Insights</a>  
                            </small>
                        </div>
                    </div>
                </div>
                <CookieConsent
                    location="bottom"
                    buttonText="Kein Problem!"
                    style={{ background: "#e7e7e7", color: "#000" }}
                    buttonStyle={{ background: "#73c573", color: "#fff", fontSize: "18px" }}>
                Wir verarbeiten Informationen über Ihren Besuch unter Einsatz von <span role="img" aria-label="cookie image">🍪</span> Cookies, um die Leistung der Website zu verbessern und das Teilen von Informationen in sozialen Netzwerken zu erleichtern. Durch die weitere Nutzung unserer Seite willigen Sie in die Nutzung dieser Cookies ein. Weitere Informationen hierzu finden Sie in unserer <a href="/datenschutzerklaerung" title="Datenschutzerklärung">Datenschutzerklärung</a>.
                </CookieConsent>
                <img src={footerIcon} alt="palmomedia" height="25" className="footerIcon" />
            </div>
        </footer>
        )
    }
}

export default Footer;