import React, { Component } from 'react';
import Nav from 'react-bootstrap/Nav';
import NavLink from 'react-bootstrap/NavLink';
import Navbar from 'react-bootstrap/Navbar';
import NavbarBrand from 'react-bootstrap/NavbarBrand';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Container from 'react-bootstrap/Container'
import axios from 'axios';
import loading from '../assets/loading.gif';
import { API_LOCATION } from '../config'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Navbar2 extends Component {
    state = {
        navbarData: null,
    };

    componentDidMount() {
        axios.get(API_LOCATION + '/sitemap').then(response => {
            this.setState({navbarData: response.data});
        });
    }

    render() {
        var menu
        if (!this.state.navbarData) { 
            menu = (
                <img src={loading} alt="loading" height="8" />
            )
        } else {
            menu = (
                this.state.navbarData.map(({post_title, url}) => <NavLink className={(url===window.location.pathname) ? 'active' : ' '} key={url} href={url} to={url}>{post_title}</NavLink>)                
            )
        }
        const returnHtml =  (
            <Navbar fixed="top" className="navbar navbar-expand-lg bg-light fixed-top bg-faded navbar-custom" expand="md">
            <Container>
                <NavbarBrand href="/">                            
                    <img src="https://www.michaelpalmer.de/images/palmomedia_logo_2020_v2.png" alt="palmomedia" className="brand" />   
                    <img src="https://cdn2.iconfinder.com/data/icons/designer-skills/128/react-256.png" height="50" alt="reactjs" />                     
                </NavbarBrand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        {menu}
                    </Nav>
                        <NavDropdown className="navbar-right" drop="left" title={
                            <FontAwesomeIcon icon={['fa', 'globe-europe']} />                            
                        } >
                            <NavDropdown.Item href="https://gitlab.com/palmomedia/version-reactjs" target="_blank">GitLab/reactjs</NavDropdown.Item>
                            <NavDropdown.Item href="https://michaelpalmer.de" target="_blank">michaelpalmer.de</NavDropdown.Item>
                            <NavDropdown.Item href="/impressum">Impressum</NavDropdown.Item>
                            <NavDropdown.Item href="/datenschutzerklaerung">Datenschutzbestimmungen</NavDropdown.Item>
                            <NavDropdown.Item href="https://api-blog.palmomedia.de/wp-admin" target="_blank">WP Admin</NavDropdown.Item>
                        </NavDropdown>                        
                </Navbar.Collapse>
            </Container>
        </Navbar>);

        return returnHtml
    }
}

export default Navbar2;