import React from "react";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { API_LOCATION } from "../config";

function createMarkup(html) {
  return { __html: html };
}

class BlogPost extends React.Component {
  state = {
    pageContent: null,
  };

  componentDidMount() {
    const { params } = this.props.match;
    var curUrl = params.slug;
    if (!curUrl || curUrl === "/" || curUrl === "") curUrl = "home";
    axios.get(API_LOCATION + "/post/" + curUrl).then((response) => {
      if (!response.data["post_content"]) {
        window.location.href = "/404";
      }
      response.data["post_content"] = response.data["post_content"].replace(
        /<p\s/g,
        '<p data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<p>/g,
        '<p data-aos="fade-right">'
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<div/g,
        '<div data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<h1/g,
        '<h1 data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<h2/g,
        '<h2 data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<h3/g,
        '<h3 data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<h4/g,
        '<h4 data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<img/g,
        '<img data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<pre\s/g,
        '<pre data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<pre>/g,
        '<pre data-aos="fade-right">'
      );
      if (response.data["post_title"]) {
        document.title = response.data["post_title"] + " | palmomedia ";
      }
      this.setState({ pageContent: response.data });
    });
  }

  render() {
    var content;
    if (!this.state.pageContent) {
      content = (
        <div style={{ textAlign: "center" }}>
          <img
            src="https://michaelpalmer.de/images/cdn/ajax-loading-icon-28.jpeg"
            height="100"
            alt="loading"
          />
        </div>
      );
    } else {
      content = (
        <div>
          <h1>{this.state.pageContent.post_title}</h1>
          <p>
            <small>Teilen:</small>
            &nbsp; &nbsp;
            <a
              className="paddingLeft removeExternalLink"
              rel="noopener noreferrer"
              href={`https://twitter.com/intent/tweet?text=%23jamstack %23devops %23webdev Schaut euch diesen Artikel ${this.state.pageContent.post_title} an: &url=https://palmomedia.de/blog/${this.state.pageContent.post_name}&`}
              target="_blank"
            >
              <FontAwesomeIcon icon={["fab", "twitter"]} />
            </a>
            <a
              className="paddingLeft removeExternalLink"
              rel="noopener noreferrer"
              href={`https://www.facebook.com/sharer/sharer.php?u=https://palmomedia.de/blog/${this.state.pageContent.post_name}`}
              target="_blank"
            >
              <FontAwesomeIcon icon={["fab", "facebook"]} />
            </a>
            <a
              className="paddingLeft removeExternalLink"
              rel="noopener noreferrer"
              href={`https://www.xing.com/spi/shares/new?url=https://palmomedia.de/blog/${this.state.pageContent.post_name}`}
              target="_blank"
            >
              <FontAwesomeIcon icon={["fab", "xing"]} />
            </a>
            <a
              className="paddingLeft removeExternalLink"
              rel="noopener noreferrer"
              href={`whatsapp://send?text=schau dir den Artikel '${this.state.pageContent.post_title}' mal an: https://palmomedia.de/blog/${this.state.pageContent.post_name}`}
              target="_blank"
            >
              <FontAwesomeIcon icon={["fab", "whatsapp"]} />
            </a>
            <a
              className="paddingLeft removeExternalLink"
              rel="noopener noreferrer"
              href={`mailto:?subject=${this.state.pageContent.post_title}&body=Hallo, schau dir mal den Artikel '${this.state.pageContent.post_title}' an: https://palmomedia.de/blog/${this.state.pageContent.post_name}`}
              target="_blank"
            >
              <FontAwesomeIcon icon={["fa", "envelope"]} />
            </a>
          </p>
          <div
            dangerouslySetInnerHTML={createMarkup(
              this.state.pageContent.post_content
            )}
          />

          <div className="d-flex flex-row-reverse" data-aos="fade-left">
            <div className="p2e">
              <div className="profile-header-container">
                <div className="profile-header-img">
                  <img
                    alt="nix"
                    className="img-circle"
                    src={this.state.pageContent.author_image}
                  />
                  <div className="rank-label-container">
                    <span className="label label-default rank-label">
                      {this.state.pageContent.display_name}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <hr />

          <a href="/blog/">
            <FontAwesomeIcon
              icon={["fa", "backward"]}
              className="marginLeftBackLink"
            />
            einmal zur&uuml;ck zu den Posts bitte...
          </a>
        </div>
      );
    }
    return <div className="content-wrapper">{content}</div>;
  }
}
export default BlogPost;
