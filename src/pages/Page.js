import React from "react";
import axios from "axios";
import { API_LOCATION } from "../config";

function createMarkup(html) {
  return { __html: html };
}

class Page extends React.Component {
  state = {
    pageContent: null,
  };

  componentDidMount() {
    const { params } = this.props.match;
    var curUrl = params.slug;
    if (!curUrl || curUrl === "/" || curUrl === "") curUrl = "home";
    axios.get(API_LOCATION + "/page/" + curUrl).then((response) => {
      if (!response.data["post_content"]) {
        window.location.href = "/404";
      }
      response.data["post_content"] = response.data["post_content"].replace(
        /<p\s/g,
        '<p data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<p>/g,
        '<p data-aos="fade-right">'
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<div/g,
        '<div data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<h1/g,
        '<h1 data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<h2/g,
        '<h2 data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<h3/g,
        '<h3 data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<h4/g,
        '<h4 data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<img/g,
        '<img data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<pre\s/g,
        '<pre data-aos="fade-right" '
      );
      response.data["post_content"] = response.data["post_content"].replace(
        /<pre>/g,
        '<pre data-aos="fade-right">'
      );
      if (response.data["post_title"]) {
        document.title = response.data["post_title"] + " | palmomedia ";
      }
      this.setState({ pageContent: response.data });
    });
  }

  render() {
    var content;
    if (!this.state.pageContent) {
      content = (
        <div style={{ textAlign: "center" }}>
          <img
            src="https://michaelpalmer.de/images/cdn/ajax-loading-icon-28.jpeg"
            height="100"
            alt="loading"
          />
        </div>
      );
    } else {
      content = (
        <div>
          <h1>{this.state.pageContent.post_title}</h1>
          <br />
          <div
            dangerouslySetInnerHTML={createMarkup(
              this.state.pageContent.post_content
            )}
          />
        </div>
      );
    }
    return <div className="content-wrapper">{content}</div>;
  }
}
export default Page;
