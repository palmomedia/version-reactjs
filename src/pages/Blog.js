import React from "react";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { API_LOCATION } from "../config";

class Blog extends React.Component {
  state = {
    pageContent: null,
  };

  componentDidMount() {
    axios.get(API_LOCATION + "/post/").then((response) => {
      document.title = "BLOG | palmomedia ";
      this.setState({ pageContent: response.data });
    });
  }

  render() {
    var content;
    if (!this.state.pageContent) {
      content = (
        <div style={{ textAlign: "center" }}>
          <img
            src="https://michaelpalmer.de/images/cdn/ajax-loading-icon-28.jpeg"
            height="100"
            alt="loading"
          />
        </div>
      );
    } else {
      content = (
        <div>
          <h1>Blog</h1>
          <hr />
          {this.state.pageContent.map(
            ({ post_title, post_name, post_excerpt }) => (
              <div key={`${post_name}`}>
                <h2>
                  <span className="badge badge-warning">{post_excerpt}</span>{" "}
                  &nbsp;
                  <a
                    href={`/blog/${post_name}`}
                    target="_self"
                    style={{ textDecoration: "none" }}
                    title={`${post_title}`}
                  >
                    {post_title}
                  </a>
                </h2>
                <p>
                  <a
                    className="paddingLeft removeExternalLink"
                    rel="noopener noreferrer"
                    href={`https://twitter.com/intent/tweet?text=%23jamstack %23devops %23webdev Schaut euch diesen Artikel an: &url=https://palmomedia.de/blog/${post_name}&`}
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={["fab", "twitter"]} />
                  </a>
                  <a
                    className="paddingLeft removeExternalLink"
                    rel="noopener noreferrer"
                    href={`https://www.facebook.com/sharer/sharer.php?u=https://palmomedia.de/blog/${post_name}`}
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={["fab", "facebook"]} />
                  </a>
                  <a
                    className="removeExternalLink"
                    rel="noopener noreferrer"
                    href={`https://www.xing.com/spi/shares/new?url=https://palmomedia.de/blog/${post_name}`}
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={["fab", "xing"]} />
                  </a>
                </p>
                <hr />
              </div>
            )
          )}

          <img
            src="https://www.michaelpalmer.de/images/work_2.jpg"
            height="250"
            alt="Papers"
            className="shadow p-3 mb-5 bg-white rounded floatRight"
          />
          <div className="clearRight" />
        </div>
      );
    }
    return <div className="content-wrapper">{content}</div>;
  }
}

export default Blog;
