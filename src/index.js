import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom'

import Page from './pages/Page';
import Blog from './pages/Blog';
import BlogPost from './pages/BlogPost';
import Footer from './pages/Footer';
import Navbar from './pages/Navbar';
import Breadcrumb from './pages/Breadcrumb';

import Notfound from './pages/notfound'
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faHome } from '@fortawesome/free-solid-svg-icons'
import { faHeartbeat } from '@fortawesome/free-solid-svg-icons'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { faBackward } from '@fortawesome/free-solid-svg-icons'
import { faGlobeEurope } from '@fortawesome/free-solid-svg-icons'

import './css/App.css';
import './css/custom.scss';

import '../node_modules/aos/dist/aos.css'; 
import AOS from 'aos'
AOS.init();

library.add(fab, faHome, faHeartbeat, faEnvelope, faBackward, faGlobeEurope)

const routing = (
    <Router>
    <div>
        <Navbar />
        <div className="content">
          <Breadcrumb />
          <Switch>
              <Route path="/404" component={Notfound} />
              <Route exact path="/blog" component={Blog} />
              <Route exact path="/blog/:slug" component={BlogPost} />
              <Route exact path="/:slug" component={Page} />
              <Route exact path="/" component={Page} />
              <Route component={Notfound} />
          </Switch>
        </div>
        <Footer />  
      </div>
    </Router>
  )

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();