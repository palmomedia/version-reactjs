# palmomedia Version React.js
![pipeline](https://gitlab.com/palmomedia/version-reactjs/badges/master/pipeline.svg?style=flat)

[![Gitter](https://badges.gitter.im/palmomedia/community.svg)](https://gitter.im/palmomedia/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

Version 2 - reacjs Version der palmomedia.de / JAMStack Webseite mit WP als Backend.
Wird es nun ReactJS oder react.js geschrieben?
https://reactjs.palmomedia.de/blog/eine-webseite-drei-frameworks-part-react-js-jamstack

## Background
Hier mehr zur Version 1 - Vue.js
https://gitlab.com/palmomedia/version-vuejs/-/blob/master/README.md

## react.js
### Komponenten
+ fortawesome
+ axios
+ bootstrap
+ react-cookie-consent
+ react-router-dom

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm start
```

### Compiles and minifies for production
```
npm build
```

### Config
Zum stehlen des "Blog Beispiels" braucht ihr nur den API Endpoint in ./config.js auszutauschen.
Näheres dazu ebenfalls in der Vue.js Variante: 
https://gitlab.com/palmomedia/version-vuejs/-/blob/master/README.md
